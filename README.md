# Validate a Sudoku

## Definition
Sudokus are number-puzzles in which cells in a 9×9 grid must be populated with the numbers `1` to `9`.

The following constraints must apply to the 9×9 grid.

 * Each digit from `1` to `9` only occurs only once 
    * per row
    * per column
    * per block (i.e. 3×3 sub grid)

![a solved simple sudoku](sudoku.gif)

## Goal
Validate, if a given Sudoku is a valid solution. 

The Sudoku must comply with the definition above.

### Input
A sequence of 81 digits, organized in 9 columns and 9 rows.

The columns of a row are separated by blanks.

Rows are separated by line-breaks.

### Output
A boolean value `true` or `false` defining the correctness of the input.

### Constraints
1 <= `digit` <= 9

### Examples
**Input:** 
```
9 8 1 5 2 3 6 4 7
6 3 4 8 7 9 2 5 1
2 7 5 1 4 6 9 8 3
1 9 6 4 8 7 5 3 2
5 4 8 3 1 2 7 6 9
7 2 3 6 9 5 4 1 8
3 1 2 7 5 4 8 9 6
4 6 9 2 3 8 1 7 5
8 5 7 9 6 1 3 2 4
```
**Output:** 
`true`
