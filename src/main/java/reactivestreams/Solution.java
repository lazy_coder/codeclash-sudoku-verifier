package reactivestreams;

import java.util.Scanner;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * stream.Solution involving a Sukdoku Board, a SudokuVerifier, plus some ReactiveStreams processing.
**/
class Solution {
	
	public static void main( String[] args ) {
		// Setup stream on System.in
		Scanner in = new Scanner( System.in );
		Spliterator spliterator = ((Iterable) () -> in).spliterator();
		Stream<String> digitStream = StreamSupport.stream(spliterator, false);
		// Setup SudokuVerifier
		SudokuVerifier verifier = new SudokuVerifier(digitStream);
		// Verify and print out result
		System.out.println(verifier.isSolved());
	}
}
