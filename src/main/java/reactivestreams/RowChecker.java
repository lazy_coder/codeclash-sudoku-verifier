package reactivestreams;

public class RowChecker extends DigitBlockChecker {

	int row;

	public RowChecker(int row) {
		this.row = row;
	}

	@Override
	protected boolean currentDigitApplies(int currentRow, int currentColumn) {
		return currentRow == row;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + row;
	}
}
