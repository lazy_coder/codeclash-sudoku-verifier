package reactivestreams;

public class SquareChecker extends DigitBlockChecker {

	int squareRow;
	int squareColumn;

	public SquareChecker(int squareRow, int squareColumn) {
		this.squareRow = squareRow;
		this.squareColumn = squareColumn;
	}

	@Override
	protected boolean currentDigitApplies(int currentRow, int currentColumn) {
		return currentRowApplies(currentRow) && currentColumnApplies(currentColumn);
	}

	boolean currentRowApplies(int currentRow) {
		int fromRow = squareRow * 3 - 2;
		int toRow = fromRow + 2;
		return currentRow >= fromRow && currentRow <= toRow;
	}

	boolean currentColumnApplies(int currentColumn) {
		int fromColumn = squareColumn * 3 - 2;
		int toColumn = fromColumn + 2;
		return currentColumn >= fromColumn && currentColumn <= toColumn;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + squareRow + "/" + squareColumn;
	}

}
