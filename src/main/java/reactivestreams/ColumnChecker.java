package reactivestreams;

public class ColumnChecker extends DigitBlockChecker {

	int column;

	public ColumnChecker(int column) {
		this.column = column;
	}

	@Override
	protected boolean currentDigitApplies(int currentRow, int currentColumn) {
		return currentColumn == column;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + column;
	}
}
