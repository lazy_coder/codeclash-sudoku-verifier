package stream;

import java.util.stream.Stream;

public class SudokuVerifier {

	public boolean isSolved(Board board) {
		return digitStreams(board)
			.allMatch(this::isSolved);
	}

	Stream<Stream<String>> digitStreams(Board board) {
		return Stream.of(board.rows(), board.columns(), board.squares())
			.flatMap(s -> s);
	}

	boolean isSolved(Stream<String> digitStream) {
		return digitStream.distinct().count() == 9;
	}

}
