package stream;

import java.util.List;
import java.util.Scanner;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * stream.Solution involving a Sukdoku Board, a SudokuVerifier, plus some Stream processing.
**/
class Solution {
	
	public static void main( String[] args ) {
		Scanner in = new Scanner( System.in );
		Spliterator spliterator = ((Iterable) () -> in).spliterator();
		List<String> digits = (List<String>) StreamSupport.stream(spliterator, false)
			.collect(Collectors.toList());
		Board board = new Board(digits);
		SudokuVerifier verifier = new SudokuVerifier();
		System.out.println(verifier.isSolved(board));
	}
}
