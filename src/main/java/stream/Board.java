package stream;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Board {

	final List<String> digits;

	public Board(List<String> digits) {
		this.digits = digits;
	}

	public Stream<Stream<String>> rows() {
		return IntStream.range(0, 9).boxed()
			.map(this::row);
	}

	public Stream<Stream<String>> columns() {
		return IntStream.range(0, 9).boxed()
			.map(this::column);
	}

	public Stream<Stream<String>> squares() {
		return IntStream.range(0, 3).boxed()
			.flatMap(this::squares);
	}

	Stream<String> row(int row) {
		return IntStream.range(0, 9).boxed()
			.map(column -> digit(row, column));
	}

	Stream<String> column(int column) {
		return IntStream.range(0, 9).boxed()
			.map(row -> digit(row, column));
	}

	Stream<Stream<String>> squares(int squareRow) {
		return IntStream.range(0, 3).boxed()
			.map(squareColumn -> square(squareRow, squareColumn));
	}

	Stream<String> square(int squareRow, int squareColumn) {
		return IntStream.range(squareRow*3, squareRow*3+3).boxed()
			.flatMap(row -> squareDigitsInRow(row, squareColumn));
	}

	Stream<String> squareDigitsInRow(int row, int squareColumn) {
		return IntStream.range(squareColumn*3, squareColumn*3+3).boxed()
			.map(column -> digit(row, column));
	}

	String digit(int row, int column) {
		int idx = row * 9 + column;
		return digits.get(idx);
	}

}
